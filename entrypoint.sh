#!/bin/bash
COMMAND="$1"

# Check if filename is provided as argument
if [[ $# -eq 2 ]]; then
  FILENAME="$2"
  # Handle different tools and flags
  if [[ "$COMMAND" == "cfn_nag_scan" ]]; then
    $COMMAND --input-path "$FILENAME"
  elif [[ "$COMMAND" == "checkov" ]]; then
    $COMMAND -d "$FILENAME"
  else
    echo "Running cfn-lint"
    $COMMAND "$FILENAME"
  fi
else
  echo "No filename provided"
  # Run the chosen command with version flag
  $COMMAND --version
fi
