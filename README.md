## README.md

**Cfn Tools Docker Image**

This repository provides a Docker image with tools for linting and validating CloudFormation templates:

* `cfn-lint`: [https://github.com/aws-cloudformation/cfn-lint](https://github.com/aws-cloudformation/cfn-lint)
* `cfn-nag`: [https://github.com/stelligent/cfn_nag](https://github.com/stelligent/cfn_nag)

**Building the Image**

1. Clone this repository:

```
git clone https://github.com/your-username/cfn-tools.git
```

2. Build the image:

```
docker build -t cfn-tools .
```

**Using the Image**

1. **Get tool version:**

```
docker run cfn-tools <tool_name> --version

# Example:
docker run cfn-tools cfn-lint --version
```

2. **Lint/Scan a CloudFormation template:**

```
docker run cfn-tools <tool_name> <filename>

# Example:
docker run cfn-tools cfn-lint my-template.yml
docker run cfn-tools cfn_nag_scan --input-path my-template.yml
```

**Optional: Adding your CloudFormation Template**

You can optionally copy your CloudFormation template file (e.g., `my-template.yml`) to the container during the build process:

```
docker build -t cfn-tools --build-arg FILE=my-template.yml .
```

Then, you can simply run the tool without specifying the filename:

```
docker run cfn-tools <tool_name>
```

**Note:** This will copy the file to the `/app` directory within the container. 

**Further notes:**

* This Dockerfile uses the `alpine` base image and creates a temporary virtual environment for the installed tools.
* The script automatically chooses the appropriate tool based on the provided command and handles flags like `--input-path` for `cfn_nag_scan`.

