FROM ruby:alpine3.19

RUN apk update && \
    apk upgrade --no-cache && \
    apk add --no-cache build-base
RUN gem install cfn-nag
RUN apk add cmd:pip3 jq git

COPY pip.conf /etc/pip/pip.conf

RUN pip3 install awscli --upgrade --quiet --break-system-packages
RUN pip3 install cfn-lint checkov --quiet --break-system-packages

WORKDIR /app

# COPY entrypoint.sh .

# ENTRYPOINT [ "sh", "entrypoint.sh" ]

# CMD ["cfn_nag_scan", "--input-path", "/app", "--output-format", "json"]
# CMD [ "cfn-lint", "--format", "json" ]
